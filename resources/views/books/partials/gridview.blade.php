@if (request()->ajax())
    <a class="btn btn-default search-button" href="{{ route('books.index') }}"> Reset Search </a>
@endif

@foreach($books->chunk($itemsPerRow) as $rowItems)
    <div class="row">
        @foreach($rowItems as $book)
            <div class="col-md-{{$bootstrapColumns}} grid-cell">
                <h4> {{ $book->name }} </h4>
                <h5> {{ $book->genre }} </h5>
                <img width="130" height="150" src="{{ asset($book->cover) }}" alt="image">
                <p> Pages {{ $book->page_count }} </p>
            </div>
        @endforeach
    </div>
@endforeach

<div class="row">
    <div class="text-center">
        {{ $books->links() }}
    </div>
</div>
