@extends('layout')

@section('content')

    @if ($books->count())
        <h2 class="text-center"> Browse </h2>

        <ul class="breadcrumb">
            <li> <a href="{{ route('books.create') }}"> Add Book </a> </li>
            <li class="active"> Books </li>
        </ul>

        <div class="row seach-panel" >
            <form class="js-search-form" action="{{ route('books.index') }}" method="GET">
                <div class="form-inline">
                    <label for="name">Seach By</label>
                    <input type="text" name="search_value" class="form-control" placeholder="Book Name, Genre">
                    <strong>or</strong>
                    <input type="text" name="from" class="form-control search-from-to" placeholder="From Pages">
                    <input type="text" name="to" class="form-control search-from-to" placeholder="To Pages">
                    <button type="submit" class="btn btn-default search-button">Search</button>
                </div>
            </form>
        </div>

        <div class="js-gridview-wapper">
            @include('books.partials.gridview',[
                'books'            => $books,
                'itemsPerRow'      => $itemsPerRow,
                'bootstrapColumns' => $bootstrapColumns,
            ])
        </div>

    @else
        <div class="col-md-12">
            <h3 class="text-center"> There is no data. </h3>
        </div>
    @endif

@endsection