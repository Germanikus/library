@extends('layout')

@section('content')

<h2 class="text-center"> Add New Book </h2>

<div class="row">
    <div class="col-md-offset-2 col-md-8">
        <form action="{{ route('books.store') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        @if (session('book-added'))
            <div class="alert alert-success js-book-added">
                <span class="close js-close">x</span>
                {{ session('book-added') }}
            </div>
        @endif

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="{{ old('name') }}" id="book-name" placeholder="Book Name">
            @if ($errors->has('name'))
                <div class="alert alert-danger"> {{ $errors->first('name') }}</div>
            @endif
        </div>

        <div class="form-group">
            <label for="sel1">Select Genre:</label>
            <select class="form-control" name="genre" id="sel1">
                @foreach($genres as $genreId => $genreName)
                    <option value="{{ $genreId }}" {{ (old('genre') == $genreId) ? 'selected' : ''}}> {{ $genreName }} </option>
                @endforeach
            </select>
            @if ($errors->has('genre'))
                <div class="alert alert-danger"> {{ $errors->first('genre') }}</div>
            @endif
        </div>

        <div class="form-group">
            <label for="upload-cover">Upload Cover</label>
            <input type="file" name="cover" id="upload-cover">
            @if ($errors->has('cover'))
                <div class="alert alert-danger"> {{ $errors->first('cover') }}</div>
            @endif
        </div>

        <div class="form-group">
            <label for="name">Page Count</label>
            <input type="text" name="page_count" value="{{ old('page_count') }}" class="form-control" id="name" placeholder="Page Count">
            @if ($errors->has('page_count'))
                <div class="alert alert-danger"> {{ $errors->first('page_count') }}</div>
            @endif

        </div>

        <button type="submit" class="btn btn-default">Submit</button>

        </form>
        <h2 class="text-center"> <a class="browse-btn" href="{{ route('books.index') }}"> Browse </a> </h2>
    </div>
</div>
@endsection