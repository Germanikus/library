<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

Route::get('/', 'BooksController@createBook')
    ->name('books.create');

Route::post('/addbook', 'BooksController@storeBook')
    ->name('books.store');

Route::get('/books', 'BooksController@index')
    ->middleware('auth')
    ->name('books.index');

Route::get('/books/search', 'BooksController@search')
    ->middleware('auth')
    ->name('books.index.search');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');