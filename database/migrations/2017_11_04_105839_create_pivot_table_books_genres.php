<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableBooksGenres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_genre', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('book_id')->unsigned()->nullable();
            $table->integer('genre_id')->unsigned()->nullable();

            $table->foreign('book_id')
                ->references('id')
                ->on('books')
                ->onDelete('set null');

            $table->foreign('genre_id')
                ->references('id')
                ->on('genres')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_genre', function(Blueprint $table) {
            $table->drop(['book_id', 'genre_id']);
            $table->dropIfExists('book_genre');
        });
    }
}
