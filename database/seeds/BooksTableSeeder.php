<?php

use App\Models\Book;
use App\Models\Genre;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $books = 24;
        for ($i = 0; $i <= $books; $i++) {
            $book = new Book;
            $book->name = $faker->name;
            $book->page_count = $faker->numberBetween(1, 2000);
            $book->save();

            $book->genres()->save(Genre::inRandomOrder()->first());
        }
    }
}
