<?php

use Illuminate\Database\Seeder;
use App\Models\Genre;

class GenresTableSeeder extends Seeder
{

    public $genres = [
        'Horror',
        'Thriller',
        'Sci-Fi',
        'Fantasy',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->genres as $genre) {
            Genre::create([
                'name' => $genre,
            ]);
        }
    }
}
