<?php

use Illuminate\Database\Seeder;
use App\Models\Cover;
use App\Models\Book;

class CoversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // http://lorempixel.com/640/480/cats/
        $books = Book::get();

        foreach ($books as $book) {
            $cover = new Cover;
            $cover->path = 'images/demoimage-437x450.jpg';
            $cover->save();

            $book->covers()->save($cover);
        }
    }
}
