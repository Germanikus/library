<?php

namespace App\Models;

use App\Models\Books;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{

    public function books()
    {
        return $this->belongsToMany(Books::class);
    }

}
