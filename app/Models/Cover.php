<?php

namespace App\Models;

use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    const PATH = 'images/covers/';

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
