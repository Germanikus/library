<?php

namespace App\Models;

use App\Models\Genre;
use App\Models\Cover;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $fillable = [
        'name',
        'page_count'
    ];

    // validation rules
    public static $validationRules = [
        'name'       => 'required',
        'page_count' => 'required|integer',
        'cover'      => 'required|max:1000|image',
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public function covers()
    {
        return $this->hasMany(Cover::class);
    }

}
