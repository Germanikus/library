<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Genre;
use App\Models\Cover;
use Illuminate\Http\Request;

class BooksController extends Controller
{

    public $itemsPerRow;
    public $bootstrapColumns;
    public $itemsPerPage;

    public function __construct()
    {
        $this->itemsPerRow = 4;
        $this->itemsPerPage = 12;
        $this->bootstrapColumns = 12 / $this->itemsPerRow;
    }

    /**
     * List all books
     *
     * @param Request $request
     * @return view
     */
    public function index(Request $request)
    {
        $books = $this->_getBooks();

        // if search results have multple pages
        if ( ! empty($request->except('page'))) {
            $books = $this->_performSearch($request, $books);
        }

        $books = $books->paginate($this->itemsPerPage)
            ->appends($request->all());

        return view('books.index', [
            'books'            => $books,
            'itemsPerRow'      => $this->itemsPerRow,
            'bootstrapColumns' => $this->bootstrapColumns,
        ]);
    }

    /**
     * Seach by user input
     *
     * @param Request $request
     * @return view
     */
    public function search(Request $request)
    {
        // if search results have multple pages
        if ( ! $request->ajax()) {
            return redirect()->route('books.index', $request->query());
        }

        $books = $this->_performSearch($request, $this->_getBooks());

        $view = view('books.partials.gridview', [
            'books'            => $books->paginate($this->itemsPerPage)->appends($request->all()),
            'itemsPerRow'      => $this->itemsPerRow,
            'bootstrapColumns' => $this->bootstrapColumns,
        ])->render();

        return response()->json([
            'html' => $view,
        ]);
    }

    /**
     * Return html form adding a book
     *
     * @return view
     */
    public function createBook()
    {
        $genres = Genre::pluck('name', 'id');

        return view('books.create', [
            'genres' => $genres
        ]);
    }

    /**
     * Adds new book into the db
     *
     * @param Request $request
     * @return redirect
     */
    public function storeBook(Request $request)
    {
        $this->validate($request, Book::$validationRules);

        $book = new Book;
        $book->name = $request->get('name');
        $book->page_count = $request->get('page_count');
        $book->save();

        $coverPath = $this->_uploadCover($request, $book->id);
        $cover = new Cover;
        $cover->path = $coverPath;
        $cover->save();
        $book->covers()->save($cover);

        $genre = Genre::find($request->get('genre'));
        $book->genres()->save($genre);

        return redirect()
            ->route('books.create')
            ->with('book-added', 'New Book Has Been Successfully Added!');
    }

    /**
     * Moves uploaded image to public/images/covers/bookId/filename
     *
     * @param  Request $request
     * @param  int $bookId
     * @return string [filepath]
     */
    private function _uploadCover(Request $request, $bookId)
    {
        $fileName = $request->cover->getClientOriginalName();

        $fileDestination = Cover::PATH . $bookId . '/';

        $request->cover->move($fileDestination, $fileName);

        return $fileDestination  . $fileName;
    }


    /**
     * Preforms a seach in db by the given params
     *
     * @param  Request $request
     * @param  builder $books
     * @return Query Builder
     * @author German Dimitrov gdimitrov@bizportal.bg
     */
    private function _performSearch(Request $request, $books)
    {
        if ($request->get('search_value')) {
            $books->where('b.name', 'like', '%' . $request->get('search_value') . '%')
                ->orWhere('g.name', 'like', '%' . $request->get('search_value'). '%');
        }

        if ($request->get('from') || $request->get('to')) {
            $to = $request->get('to');
            $from = $request->get('from');

            if (empty($to)) {
                $to = Book::max('page_count');
            }

            if (empty($from)) {
                $from = Book::min('page_count');
            }

            $books->whereBetween('b.page_count', [$from, $to]);
        }

        return $books;
    }

    /**
     * Returns books with covers, genre ordered desc
     *
     * @return QueryBuilder
     */
    private function _getBooks()
    {
        $books = Book::from('books as b')
            ->select([
                'b.id',
                'b.name',
                'b.page_count',
                'c.path as cover',
                'g.name as genre',
            ])
            ->leftjoin('covers as c', 'c.book_id', '=', 'b.id')
            ->leftjoin('book_genre as bg', 'bg.book_id', '=', 'b.id')
            ->leftjoin('genres as g', 'bg.genre_id', '=', 'g.id')
            ->orderBy('b.created_at', 'desc');

        return $books;
    }
}
