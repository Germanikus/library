(function() {
    var baseUrl = window.location.origin;

    $('.js-search-form').on('submit', function(event) {
        event.preventDefault();
        var formData = $(this).serializeArray();

        $.ajax({
            'url': baseUrl + '/books/search',
            'method': 'get',
            'data': formData,
            'success': function(data) {
                $('.js-gridview-wapper').html(data.html);
            },
            'error': function() {
                console.log('error');
            }
        });
    });

    $('.js-close').on('click', function() {
        $('.js-book-added').fadeOut(200);
    });

    // $('.js-book-added').fadeOut(5000);
})();